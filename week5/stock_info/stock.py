from flask import Flask, render_template, request
from alpha_vantage_data import get_stock_data
from config import api_key

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    current = {}
    fundamentals = {}
    stock_news = {}
    stock_ts = {}
    
    if request.method == 'POST':
        symbol = request.form['stock_symbol']
        current, fundamentals, stock_news, stock_ts = get_stock_data(api_key,symbol)

    return render_template('stock.html',current=current, fundamentals=fundamentals, stock_news=stock_news, stock_ts=stock_ts)


if __name__ == '__main__':
    app.run()
