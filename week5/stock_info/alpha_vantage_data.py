import requests
import datetime
from config import api_key
import pandas as pd

def get_current(api_key,symbol):
    response = requests.get('https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol={}&interval=5min&apikey={}'.format(symbol,api_key))
    data = response.json()
    info = dict()
    time = data["Meta Data"]["3. Last Refreshed"]
    tsdata = data["Time Series (5min)"]
    prices = tsdata[time]
    info['current_price'] = prices["4. close"]
    info['open'] = prices["1. open"]
    info['volume'] = prices["5. volume"]
    
    time = datetime.datetime.strptime(time,'%Y-%m-%d %H:%M:%S')
    prev_time = time-datetime.timedelta(minutes=5)
    prev_time = prev_time.strftime('%Y-%m-%d %H:%M:%S')
    while prev_time not in tsdata:
        prev_time = datetime.datetime.strptime(prev_time,'%Y-%m-%d %H:%M:%S')-datetime.timedelta(minutes=5)
        prev_time = prev_time.strftime('%Y-%m-%d %H:%M:%S')
    info['prev_close'] = tsdata[prev_time]["4. close"]

    return info

def get_fundamentals(api_key,symbol):
    response = requests.get('https://www.alphavantage.co/query?function=OVERVIEW&symbol={}&apikey={}'.format(symbol,api_key))
    data = response.json()
    info = dict()
    info['StockSymbol'] = symbol
    info['CompanyName'] = data['Name']
    info['Sector'] = data['Sector']
    info['Industry'] = data['Industry']
    info['MarketCap'] = data["MarketCapitalization"]
    info['PE'] = data['PERatio']
    info['EPS'] = data['EPS']
    info['DividendPerShare'] = data["DividendPerShare"]
    info['DividendYield'] = data["DividendYield"]
    info['Exchange'] = data['Exchange']
    info['52WeekHigh'] = data['52WeekHigh']
    info['52WeekLow'] = data['52WeekLow']
    return info

def get_news(api_key,symbol):
    response = requests.get('https://www.alphavantage.co/query?function=NEWS_SENTIMENT&tickers={}&apikey={}'.format(symbol,api_key))
    data = response.json()
    data = data['feed']
    data = data[:5]
    info = []

    for i in range(5):
        info.append(dict())
        info[i]['title'] = data[i]['title']
        info[i]['url'] = data[i]['url']
        info[i]['summary'] = data[i]['summary']
        info[i]['sentiment'] = data[i]['overall_sentiment_label']

    return info

def get_stock_ts(api_key,symbol):
    response = requests.get('https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol={}&apikey={}&outputsize=full'.format(symbol,api_key))
    data = response.json()
    current_time = data["Meta Data"]["3. Last Refreshed"][:10]
    current_time = datetime.datetime.strptime(current_time,'%Y-%m-%d')
    start_time = current_time - datetime.timedelta(days=365)
    start_time = datetime.datetime.strftime(start_time,'%Y-%m-%d')
    info = dict({'X':[],'Y':[]})

    ts_data = data["Time Series (Daily)"]

    for i in pd.date_range(start_time,current_time,freq='D'):
        n = datetime.datetime.strftime(i,'%Y-%m-%d')
        if n in ts_data:
            info['X'].append(n)
            prices = ts_data[n]["4. close"]
            info['Y'].append(prices)
    return info

def get_stock_data(api_key,symbol):
    current = get_current(api_key, symbol)
    fundamentals = get_fundamentals(api_key,symbol)
    stock_news = get_news(api_key,symbol)
    stock_ts = get_stock_ts(api_key,symbol)
    return current, fundamentals, stock_news, stock_ts
