from flask import Blueprint, flash, g, redirect, render_template, request, url_for, Flask
from werkzeug.exceptions import abort
from flaskr.auth import login_required
from flaskr.db import get_db
import requests
from datetime import datetime
from stock_tracker import config
from stock_tracker.db import *
from flask import Blueprint

# app = Flask(__name__)
bp = Blueprint("stock", __name__)
# app.register_blueprint(bp)

@bp.route('/', methods=['GET', 'POST'])
@login_required
def index():
    if request.method == 'POST':
        symbol = request.form['symbol']
        tracking_price = float(request.form['tracking_price'])
        shares = int(request.form['num_shares'])
        now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        print(now)
        db = get_db()
        db.execute('INSERT INTO stocks (symbol, tracking_price, shares, datetime_added) VALUES (?, ?, ?, ?)', (symbol, tracking_price, shares, now))
        db.commit()
        return redirect(url_for('index'))
    else:
        db = get_db()
        cursor = db.execute('SELECT * FROM stocks')
        rows = cursor.fetchall()
        stocks = []
        for row in rows:
            id = row['id']
            symbol = row['symbol']
            tracking_price = row['tracking_price']
            current_price = get_stock_price(symbol)
            percent_change = round((current_price - tracking_price) / tracking_price * 100, 3)
            shares = row['shares']
            datetime_added = row['datetime_added']
            stocks.append({
                'id':id,
                'symbol': symbol,
                'tracking_price': tracking_price,
                'current_price': current_price,
                'percent_change': percent_change,
                'shares': shares,
                'datetime_added': datetime_added
            })
        return render_template('index.html', stocks=stocks)


def get_stock_price(symbol):
    response = requests.get(f'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol={symbol}&interval=5min&apikey={config.api_key}')
    data = response.json()
    time = data["Meta Data"]["3. Last Refreshed"]
    tsdata = data["Time Series (5min)"]
    price = tsdata[time]
    return float(price["4. close"])


@bp.route("/<int:id>/delete", methods=["POST"])
@login_required
def delete(id):
    db = get_db()
    db.execute('DELETE FROM stocks WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('index'))

# if __name__ == '__main__':
#     app.run()